import logo from './logo.svg';
import './App.css';
import React, {useState, useEffect} from 'react';

// Import the functions you need from the SDKs you need
//import { initializeApp } from "firebase/app";
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.15.0/firebase-app.js";
     
//import { getAnalytics, logEvent } from "firebase/analytics";
import { getAnalytics, logEvent } from "https://www.gstatic.com/firebasejs/9.15.0/firebase-analytics.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAVXXVZj8RIdZG13QQuGaUDkYCGpMrLzHM",
  authDomain: "workpaluat.firebaseapp.com",
  projectId: "workpaluat",
  storageBucket: "workpaluat.appspot.com",
  messagingSenderId: "712118148846",
  appId: "1:712118148846:web:e8224a263c4764132d5dff",
  measurementId: "G-X9XKB5Z54T"
};


// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

function App() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    // Update the document title using the browser API
    document.title = `You clicked ${count} times`;
  });

  const onButtonPress = (params) => {
    setCount(count + 1);
    logEvent(analytics ,'issue_clicked', { name: 'hiii', issue: params, date: new Date() });
    };

  return (
    <div>
   
      <p>You clicked {count} times</p>
      <br></br>
      <button id='1' onClick={()=> onButtonPress('issue A')}>
        Issue A
      </button>
    <br></br>
      <button id='2' onClick={()=> onButtonPress('issue B')}>
        Issue B
      </button>
<br></br>
      <button id='3' onClick={()=>onButtonPress('issue C')}>
        Issue C
      </button>

    </div>
  );
  
}

export default App;
